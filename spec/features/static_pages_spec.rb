require 'spec_helper'

describe "StaticPages" do

  subject { page }

  describe "Home Page" do
    before { visit root_path }  
    it { should have_content 'Sample App' }
    it { should have_title full_title('') }
    it { should_not have_title " | Home" }
    it "should visit right link on home page" do
      click_link "About"
      page.should have_content 'About us'
      click_link "Home"
      page.should have_content 'Sample App'
      click_link "Help"
      page.should have_content 'Help'
      click_link "Contact"
      page.should have_content 'Contact'
    end
  end

  describe "Help Page" do
    before { visit help_path }
    it { should have_content 'Help' }
    it { should have_title full_title('Help') }
  end

  describe "About us" do
    before { visit about_path }
    it { should have_content 'About us' }
    it { should have_title full_title('About') }
  end

  describe "Contact" do
    before { visit contact_path }
    it { should have_content 'Contact' }
    it { should have_title full_title('Contact') }
  end

end
