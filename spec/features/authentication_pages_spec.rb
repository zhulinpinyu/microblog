require 'spec_helper'

describe "AuthenticationPages" do
  subject { page }
  
  describe "signin page" do
    before { visit signin_path }
    it { should have_selector('h1', text: 'Sign in') }
    it { should have_title 'Sign in' }
  end
  
  describe "signin" do
    before { visit signin_path }
    
    describe "with invalid information" do
      before { click_button "Sign in" }
      it { should have_title "Sign in" }
      it { should have_selector('div.alert.alert-error', text: 'Invalid') }
      
      describe "after visiting another page" do
        before { click_link "Home" }
        it { should_not have_selector('div.alert.alert-error') }
      end
    end

    describe "with valid information" do
      let(:user) { FactoryGirl.create(:user) }
      
      before { sign_in user }

      it { should have_title user.name }
      it { should have_link( 'Users', href: users_path) }
      it { should have_link( 'Profile', href: user_path(user)) }
      it { should have_link( 'Settings', href: edit_user_path(user)) }
      it { should have_link( 'Sign out', href: signout_path) }
      it { should_not  have_link( 'Sign in', href: signin_path) }
      it { should have_selector('div.alert.alert-success', text: "Welcome to Microblog,#{user.name}") }
      
      describe "followed by signout" do
        before { click_link "Sign out" }
        it { should have_link 'Sign in' }
      end
    end
  end

  describe "authentication" do
      
    describe "for non-signed-in users" do
      let(:user) {FactoryGirl.create(:user)}

      describe "when attempting to visit a protected page" do
        before do
          visit edit_user_path(user)
          sign_in user
        end

        describe "after signing in" do
          it "should render the desired protected page" do
            page.should have_title "Edit user"
          end
        end
      end

      describe "in the Users controller" do
        describe "visit edit page" do
          before {visit edit_user_path(user)}
          it { should have_title "Sign in" }
        end

        # this test can not work(undefined method 'put') 
        # describe "submitting to the update action" do
        #   before { put user_path(user) }
        #   specify { response.should redirect_to(signin_path)}
        # end

        describe "visiting the user index" do
          before {visit users_path}
          it {should have_title "Sign in"}
        end
      end
    end

    describe "as a wrong user" do
      let(:user) { FactoryGirl.create(:user) }
      let(:otheruser) { FactoryGirl.create(:user, email: "otheruser@gmail.com") }

      before { sign_in user }

      describe "visiting Users#edit page" do
        before {visit edit_user_path(otheruser)}
        it {should_not have_title "Edit user"}
      end

      #test update action (but 'put' method can not work)
    end
  
    # describe "as non-admin user" do
    #   let(:user) { FactoryGirl.create(:user) }
    #   let(:non_admin) { FactoryGirl.create(:user) }
      
    #   before { sign_in non_admin }

    #   describe "submitting a DELETE request to the Users#destroy action" do
    #     before { delete user_path(user) }
    #     specify { response.should redirect_to(root_path) }
    #   end
    # end
  end
end
