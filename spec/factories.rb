FactoryGirl.define do
	factory :user do
		sequence(:name) { |n| "zhulinpinyu#{n}" }
		sequence(:email) { |n| "zhulinpinyu_#{n}@gmail.com" }   
		password "zhulinpinyu"
		password_confirmation "zhulinpinyu"
    factory :admin do
      admin true
    end
	end
end