namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    admin = User.create!(name: "zhulinpinyu",
                 email: "mulixiang199016@gmail.com",
                 password: "mlx521brj",
                 password_confirmation: "mlx521brj")
    admin.toggle!(:admin)
    99.times do |n|
      name = Faker::Name.name
      email = "example-#{n+1}@gmail.com"
      password = "example"
      User.create!(name: name,
                   email: email,
                   password: password,
                   password_confirmation: password)
    end
  end
end